# kube-app

Simple demonstration of a CI/CD pipeline using Docker + Kubernetes


This pipeline builds a new docker container pushes it to the repository and then uses goss to test it, if the test is sucessfull it will deploy the application on the kubernetes cluster.

To configure the integration between gitlab and k8s please utilize the Gitlab documentation.
